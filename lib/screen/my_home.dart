import 'package:baby_names/model/baby_names.dart';
import 'package:baby_names/screen/sugestao.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: StreamBuilder<QuerySnapshot<BabyName>>(
            stream: FirebaseFirestore.instance
                .collection('baby_names')
                .withConverter<BabyName>(
                    fromFirestore: (snapshot, _) {
                      return BabyName.fromJson(snapshot.data()!);
                    },
                    toFirestore: (model, _) => model.toJson())
                .orderBy("count", descending: true)
                .snapshots(),
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.waiting:
                case ConnectionState.none:
                  return CircularProgressIndicator();
                case ConnectionState.active:
                case ConnectionState.done:
                  if (snapshot.hasData) {
                    return ListView(
                        children: snapshot.data!.docs
                            .map((e) => ListTile(
                                  title: Text(e.data().name),
                                  subtitle: Text("${e.data().count} votos"),
                                  trailing: ElevatedButton(
                                      child: Text("+1"),
                                      onPressed: () => e.reference.update(
                                          {"count": FieldValue.increment(1)})),
                                ))
                            .toList());
                  }
              }
              return Text("Nothing Found");
            }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.of(context).pushNamed(SugestaoScreen.route),
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
