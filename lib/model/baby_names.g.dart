// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'baby_names.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BabyName _$BabyNameFromJson(Map<String, dynamic> json) => BabyName(
      count: json['count'] as int? ?? 0,
      name: json['name'] as String,
    );

Map<String, dynamic> _$BabyNameToJson(BabyName instance) => <String, dynamic>{
      'count': instance.count,
      'name': instance.name,
    };
